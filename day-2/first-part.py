h_pos = 0
depth = 0

with open("input.txt", 'r') as f:
    for line in f:
        a, b = line.split(' ')
        if(a[0] == 'f'):
            h_pos += int(b[0])
        elif(a[0] == 'd'):
            depth += int(b[0])
        else:
            depth -= int(b[0])

print(depth * h_pos)

