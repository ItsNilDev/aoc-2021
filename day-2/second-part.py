h_pos = 0
depth = 0
aim = 0

with open("input.txt", 'r') as f:
    for line in f:
        a, b = line.split(' ')
        b = int(b)
        if(a[0] == 'f'):
            h_pos += b
            depth += aim * b
        elif(a[0] == 'd'):
            aim += b
        else:
            aim -= b

print(depth * h_pos)
