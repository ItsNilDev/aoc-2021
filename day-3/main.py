
def part_one(numbers: list) -> int:
    gama = ""
    epsilon = ""
    for t in range(len(numbers[0])):
        ones  = 0
        zeros = 0
        for i in numbers:
            if i[t] == '1':
                ones += 1
            else:
                zeros += 1
        if ones > zeros:
            gama += '1'
            epsilon += '0'
        else:
            gama += '0'
            epsilon += '1'
    return int(gama, 2) * int(epsilon, 2)

def part_two(numbers: list) -> int:
    def ogr(numbers1: list, t: int) -> list:
        if len(numbers1) == 1:
            return int(numbers1[0], 2)
        new_numbers = []
        ones  = 0
        zeros = 0
        for i in numbers1:
            if i[t] == '1':
                ones += 1
            elif i[t] == '0':
                zeros += 1
        if ones > zeros or ones == zeros:
            for i in numbers1:
                if i[t] == '1':
                    new_numbers.append(i)
        elif ones < zeros:
            for i in numbers1:
                if i[t] == '0':
                    new_numbers.append(i)
        return new_numbers
    def csr(numbers1: list, t: int) -> list:
        if len(numbers1) == 1:
            return int(numbers1[0], 2)
        new_numbers = []
        ones  = 0
        zeros = 0
        for i in numbers1:
            if i[t] == '1':
                ones += 1
            elif i[t] == '0':
                zeros += 1
        if ones > zeros or ones == zeros:
            for i in numbers1:
                if i[t] == '0':
                    new_numbers.append(i)
        elif ones < zeros:
            for i in numbers1:
                if i[t] == '1':
                    new_numbers.append(i)
        return new_numbers

    t = 0
    a = ogr(numbers, t)
    for f in range(len(numbers[0])):
        t += 1
        a = ogr(a, t)
        if isinstance(a, int):
            break
    t = 0
    b = csr(numbers, t)
    for f in range(len(numbers[0])):
        t += 1
        b = csr(b, t)
        if isinstance(b, int):
            break
    return a * b


numbers = []
with open("input.txt", 'r') as f:
    numbers = str(f.read()).split('\n')
    numbers.pop()

# print(part_one(numbers))
print(part_two(numbers))

